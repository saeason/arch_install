#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
hwclock --systohc
sed -i '154s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_GB.UTF-8" >> /etc/locale.conf
echo "KEYMAP=uk-latin1" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | chpasswd # >>>>>> change password here <<<<<<<

sudo pacman --needed --ask 4 -Sy - < base_packages.txt

grub-install --target=i386-pc /dev/sdX # >>>>>> replace sdx with disk name, not the partition <<<<<<
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable acpid.service
systemctl enable firewalld.service
systemctl enable NetworkManager.service
systemctl enable sshd.service
# systemctl enable vboxservice.service

sed -i '/CheckSpace/a \ILoveCandy\' /etc/pacman.conf
sed -i 's/^#Color/Color/' /etc/pacman.conf
sed -i 's/^#Para/Para/' /etc/pacman.conf

useradd -mG wheel -s /bin/bash scott
echo scott:password | chpasswd # >>>>>> change password here as well <<<<<<

echo "scott ALL=(ALL) ALL" >> /etc/sudoers.d/scott

printf "\e[1;32mDone! Type visudo and uncomment 'wheel ALL=(ALL) ALL' then exit, umount -a and reboot.\e[0m"
